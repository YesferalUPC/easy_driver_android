package yesferal.cueva.com.easy_driver_android.Util;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import yesferal.cueva.com.easy_driver_android.Model.Travel;
import yesferal.cueva.com.easy_driver_android.R;

/**
 * Created by yesferal on 18/06/16.
 */
public class MyTravelAdapter extends RecyclerView.Adapter<MyTravelAdapter.MyTravelViewHolder> {

    private ArrayList<Travel> item;

    public MyTravelAdapter(ArrayList<Travel> item) {
        this.item = item;
    }

    @Override
    public MyTravelViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.drawer_my_travel,parent,false);
        MyTravelViewHolder viewHolder = new MyTravelViewHolder(v);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(MyTravelViewHolder holder, int position) {
        holder.title.setText(item.get(position).getTitle());
        holder.description.setText(item.get(position).getDescription());
        holder.image.setImageResource(item.get(position).getImage());
    }


    @Override
    public int getItemCount() {
        return item.size();
    }

    public class MyTravelViewHolder extends RecyclerView.ViewHolder{

        public TextView title,description;
        public ImageView image;
        public MyTravelViewHolder(View itemView) {
            super(itemView);

            title = (TextView) itemView.findViewById(R.id.TV_List_title);
            description = (TextView) itemView.findViewById(R.id.TV_List_desc);
            image = (ImageView) itemView.findViewById(R.id.IV_List_avatar);

        }
    }

}

