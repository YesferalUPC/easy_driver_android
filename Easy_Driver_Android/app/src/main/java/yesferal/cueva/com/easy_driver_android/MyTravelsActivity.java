package yesferal.cueva.com.easy_driver_android;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;

import java.util.ArrayList;

import yesferal.cueva.com.easy_driver_android.Model.Travel;
import yesferal.cueva.com.easy_driver_android.Util.MyTravelAdapter;

public class MyTravelsActivity extends AppCompatActivity {

    private RecyclerView recyclerView;
    private RecyclerView.LayoutManager layoutManager;
    private RecyclerView.Adapter adapter;

    private ArrayList<Travel> datos;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_travels);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        datos = new ArrayList<Travel>();
        datos.add(new Travel("Primer Viaje",R.drawable.ic_motorcycle_48, "Se viajo desde UPC hasta mi casa"));
        datos.add(new Travel("Segundo Viaje",R.drawable.ic_motorcycle_48, "Se viajo de Barranco hasta miraflores"));
        datos.add(new Travel("Tercer Viaje",R.drawable.ic_motorcycle_48, "Se viajo hasta la cebiheria mi barrunto"));
        datos.add(new Travel("Cuarto Viaje",R.drawable.ic_motorcycle_48, "Se viajo desde mi casa hasta la UPC"));
        datos.add(new Travel("Quinto Viaje",R.drawable.ic_motorcycle_48, "Se viajo desde Miraflores hasta San Isidro"));

        recyclerView = (RecyclerView) findViewById(R.id.my_recycler_view);
        layoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        adapter = new MyTravelAdapter(datos);

        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(adapter);
    }

}
