package yesferal.cueva.com.easy_driver_android.Model;

/**
 * Created by yesferal on 18/06/16.
 */
public class Travel {
    private int image;
    private String description;
    private String title;

    public Travel(String title, int image, String description) {
        this.title = title;
        this.image = image;
        this.description = description;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getImage() {
        return image;
    }

    public void setImage(int image) {
        this.image = image;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
