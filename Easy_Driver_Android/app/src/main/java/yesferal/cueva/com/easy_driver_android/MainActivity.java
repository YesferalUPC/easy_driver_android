package yesferal.cueva.com.easy_driver_android;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;


//import com.firebase.client.AuthData;
//import com.firebase.client.Firebase;7
//import com.firebase.client.FirebaseError;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import yesferal.cueva.com.easy_driver_android.Util.Util;

public class MainActivity extends AppCompatActivity {
    private static final String TAG = "LoginActivity";
    private static final int REQUEST_SIGNUP = 0;


    EditText et_Email;
    EditText et_Password;
    Button btn_Login;
    TextView tv_SingUp;
    private FirebaseAuth mAuth;
    private FirebaseAuth.AuthStateListener mAuthListener;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //Firebase.setAndroidContext(this);

        // CODIGO FIREBASE
        mAuth = FirebaseAuth.getInstance();
        mAuthListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                FirebaseUser user = firebaseAuth.getCurrentUser();
                if (user != null) {
                    // User is signed in
                    Log.d(TAG, "onAuthStateChanged:signed_in:" + user.getUid());
                } else {
                    // User is signed out
                    Log.d(TAG, "onAuthStateChanged:signed_out");
                }
                // ...
            }
        };


        et_Email = (EditText)  findViewById(R.id.ET_Email);
        et_Password = (EditText)  findViewById(R.id.ET_Password);
        btn_Login = (Button)  findViewById(R.id.BTN_Login);
        tv_SingUp = (TextView) findViewById(R.id.TV_SingUp);

        btn_Login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                login();
                //Snackbar.make(v, "login !", Snackbar.LENGTH_LONG)
                // .setAction("Action", null).show();
            }
        });

        tv_SingUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Intent intent = new Intent(getApplicationContext(), SignupActivity.class);
                //startActivityForResult(intent, REQUEST_SIGNUP);
                Snackbar.make(v, "New Accountant", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });
    }

    public void login() {
        Log.d(TAG, "Login");


        String email = et_Email.getText().toString();
        String password = et_Password.getText().toString();

        mAuth.signInWithEmailAndPassword(email, password)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>(){
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        Log.d(TAG, "signInWithEmail:onComplete:" + task.isSuccessful());
                        if (task.isSuccessful()) {
                            btn_Login.setEnabled(false);

                            final ProgressDialog progressDialog = new ProgressDialog(MainActivity.this,
                                    R.style.AppTheme_Dark_Dialog);
                            progressDialog.setIndeterminate(true);
                            progressDialog.setMessage("Autenticando...");
                            progressDialog.show();


                            // TODO: Implement your own authentication logic here.

                            new android.os.Handler().postDelayed(
                                    new Runnable() {
                                        public void run() {
                                            // On complete call either onLoginSuccess or onLoginFailed
                                            onLoginSuccess();
                                            // onLoginFailed();
                                            progressDialog.dismiss();
                                        }
                                    }, 3000);

                            btn_Login.setEnabled(true);
                        }
                        if (!task.isSuccessful()) {
                            Log.w(TAG, "signInWithEmail- Error : "+ task.getException());
                            String myPersonalizeMessage = "Vuelva a intentarlo";
                            /*switch (task.getException()) {
                                case FirebaseError.INVALID_EMAIL:
                                    // handle a non existing user
                                    myPersonalizeMessage = "Email no valido; prueba con otro nuevo email";
                                    break;
                                case FirebaseError.INVALID_PASSWORD:
                                    // handle an invalid password
                                    myPersonalizeMessage = "Password no valida, prueba otra contraseña";
                                    break;
                                case FirebaseError.NETWORK_ERROR:
                                    // handle an invalid password
                                    myPersonalizeMessage = "Problema de conexion, vuelve a intentarlo por favor";
                                    break;
                                case FirebaseError.EMAIL_TAKEN:
                                    // handle an invalid password
                                    myPersonalizeMessage = "Email ya registrado; prueba con otro email";
                                    break;
                                case FirebaseError.USER_DOES_NOT_EXIST:
                                    // handle an invalid password
                                    myPersonalizeMessage = "Usuario invalido; prueba con otro usuario";
                                    break;
                                default:
                                    // handle other errors
                                    myPersonalizeMessage = "Ocurrio un error";
                                    break;
                            }*/
                            Snackbar.make(getCurrentFocus(), myPersonalizeMessage, Snackbar.LENGTH_LONG)
                                    .setAction("Action", null)
                                    .show();
                        }

                    }
                });

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_SIGNUP) {
            if (resultCode == RESULT_OK) {

                // TODO: Implement successful signup logic here
                // By default we just finish the Activity and log them in automatically
                this.finish();
            }
        }
    }

    @Override
    public void onBackPressed() {
        // disable going back to the MainActivity
        moveTaskToBack(true);
    }

    public void onLoginSuccess() {
        btn_Login.setEnabled(true);
        Intent intent = new Intent(this, DashboardActivity.class);
        startActivity(intent);
        et_Password.setText("");
        et_Password.requestFocus();
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (mAuthListener != null) {
            mAuth.removeAuthStateListener(mAuthListener);
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        mAuth.addAuthStateListener(mAuthListener);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
